# postfix

Configure postfix

## Requirements

Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.

## Role Variables

```
cts_role_postfix:
  soft_bounce: no               (default: no)
  myhostname: www.exemple.tld   (default: FQDN)
  mydestination: exemple.tld    (default: FQDN)
  mynetworks:                   (default: localhost and server IPs)
    - 192.168.0.0/24
  inet_protocols: ipv4          (default: ipv4)
  interfaces: loopback-only     (default: loopback-only)
  relayhost: smtp.exemple.tld   (default: null)

cts_role_postfix_aliases:
  'www-data': ['/dev/null']
  'root': ['admisec']

```

## Dependencies

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

## Example Playbook

```
- hosts: all
  gather_facts: False
  pre_tasks:
    - name: pre_tasks | setup python
      raw: command -v yum >/dev/null && yum -y install python python-simplejson libselinux-python redhat-lsb || true ; command -v apt-get >/dev/null && sed -i '/cdrom/d' /etc/apt/sources.list && apt-get update && apt-get install -y python python-simplejson lsb-release aptitude || true
      changed_when: False
    - name: pre_tasks | gets facts
      setup:
      tags:
      - always

  roles:
    - postfix
```

